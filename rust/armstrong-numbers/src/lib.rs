pub fn is_armstrong_number(num: u32) -> bool {
    let mut remaining = num;
    let mut digits = Vec::new();
    while remaining > 0 {
        digits.push(remaining % 10);
        remaining /= 10;
    }
    let count = digits.len() as u32;
    num as u64 == digits.into_iter().map(|d| (d as u64).pow(count)).sum()
}
