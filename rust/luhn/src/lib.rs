use unicode_segmentation::UnicodeSegmentation;

/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    code.graphemes(true)
        .filter(|value| value != &" ")
        .take(2)
        .count()
        > 1
        && code
            .graphemes(true)
            .filter(|value| value != &" ")
            .map(str::parse::<i32>)
            .map(|value| value.map_or(None, |value| if value > 9 { None } else { Some(value) }))
            .rev()
            .enumerate()
            .map(|(index, value)| value.map(|value| if index % 2 == 0 { value } else { value * 2 }))
            .map(|value| value.map(|value| if value > 9 { value - 9 } else { value }))
            .sum::<Option<i32>>()
            .map_or(false, |sum| sum % 10 == 0)
}
