use unicode_segmentation::UnicodeSegmentation;

#[derive(PartialEq)]
enum BracketType {
    Bracket,
    Brace,
    Parenthesis,
}

impl BracketType {
    fn open(&self) -> &str {
        match self {
            BracketType::Bracket => "[",
            BracketType::Brace => "{",
            BracketType::Parenthesis => "(",
        }
    }

    fn close(&self) -> &str {
        match self {
            BracketType::Bracket => "]",
            BracketType::Brace => "}",
            BracketType::Parenthesis => ")",
        }
    }
}

const ALL_BRACKET_TYPES: [BracketType; 3] = [
    BracketType::Bracket,
    BracketType::Brace,
    BracketType::Parenthesis,
];

pub fn brackets_are_balanced(string: &str) -> bool {
    let mut open_brackets = Vec::new();
    for g in string.graphemes(true) {
        for bracket in ALL_BRACKET_TYPES {
            if g == bracket.open() {
                open_brackets.push(bracket);
            } else if g == bracket.close() && open_brackets.pop() != Some(bracket) {
                return false;
            }
        }
    }
    open_brackets.is_empty()
}
