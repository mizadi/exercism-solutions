pub fn verse(n: u32) -> String {
    format!(
        "{0} bottle{2} of beer on the wall, {1} bottle{2} of beer.\n{3}\n",
        match n {
            0 => "No more".to_string(),
            n => n.to_string(),
        },
        match n {
            0 => "no more".to_string(),
            n => n.to_string(),
        },
        match n {
            1 => "",
            _ => "s",
        },
        match n {
            0 => "Go to the store and buy some more, 99 bottles of beer on the wall.".to_string(),
            n => format!(
                "Take {} down and pass it around, {} bottle{} of beer on the wall.",
                match n {
                    1 => "it",
                    _ => "one",
                },
                match n - 1 {
                    0 => "no more".to_string(),
                    n => n.to_string(),
                },
                match n - 1 {
                    1 => "",
                    _ => "s",
                }
            ),
        }
    )
}

pub fn sing(start: u32, end: u32) -> String {
    (end..=start)
        .rev()
        .map(verse)
        .collect::<Vec<_>>()
        .join("\n")
}
