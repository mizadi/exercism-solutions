pub fn factors(n: u64) -> Vec<u64> {
    let mut rem = n;

    // Iterate over every other element of [2, 2, 3, 4, 5, 6, ...] which means 2 and every odd
    // number after it.
    let mut result = [2]
        .into_iter()
        .chain(2..)
        .step_by(2)
        .take_while(|k| k * k <= n)
        .fold(Vec::new(), |mut result, k| {
            while rem % k == 0 {
                result.push(k);
                rem /= k;
            }
            result
        });

    // if `rem` is greater than 1, then it is not divisible by any of the (prime) numbers up until
    // square root of `n`, so it is a prime number itself and is a factor of `n`.
    if rem > 1 {
        result.push(rem);
    }
    result
}
