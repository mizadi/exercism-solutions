use std::cmp::Ordering;

/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.
pub fn winning_hands<'a>(hands: &[&'a str]) -> Vec<&'a str> {
    let mut hands = hands.iter().map(|hand| Hand::from_text(hand));
    let mut result = vec![];
    if let Some(hand) = hands.next() {
        result = vec![hand];
        for hand in hands {
            match hand.cmp(&result[0]) {
                Ordering::Equal => result.push(hand),
                Ordering::Greater => result = vec![hand],
                Ordering::Less => (),
            }
        }
    }
    result.iter().map(|hand| hand.text).collect()
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Copy)]
enum Rank {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

impl Rank {
    fn from_text(text: &str) -> Self {
        match text {
            "2" => Rank::Two,
            "3" => Rank::Three,
            "4" => Rank::Four,
            "5" => Rank::Five,
            "6" => Rank::Six,
            "7" => Rank::Seven,
            "8" => Rank::Eight,
            "9" => Rank::Nine,
            "10" => Rank::Ten,
            "J" => Rank::Jack,
            "Q" => Rank::Queen,
            "K" => Rank::King,
            "A" => Rank::Ace,
            _ => panic!(),
        }
    }

    fn from_index(index: usize) -> Self {
        match index {
            0 => Rank::Two,
            1 => Rank::Three,
            2 => Rank::Four,
            3 => Rank::Five,
            4 => Rank::Six,
            5 => Rank::Seven,
            6 => Rank::Eight,
            7 => Rank::Nine,
            8 => Rank::Ten,
            9 => Rank::Jack,
            10 => Rank::Queen,
            11 => Rank::King,
            12 => Rank::Ace,
            _ => panic!(),
        }
    }

    fn to_index(self) -> usize {
        match self {
            Rank::Two => 0,
            Rank::Three => 1,
            Rank::Four => 2,
            Rank::Five => 3,
            Rank::Six => 4,
            Rank::Seven => 5,
            Rank::Eight => 6,
            Rank::Nine => 7,
            Rank::Ten => 8,
            Rank::Jack => 9,
            Rank::Queen => 10,
            Rank::King => 11,
            Rank::Ace => 12,
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd)]
enum Suit {
    Club,
    Diamond,
    Heart,
    Spade,
}

impl Suit {
    fn from_text(text: &str) -> Self {
        match text {
            "C" => Suit::Club,
            "D" => Suit::Diamond,
            "H" => Suit::Heart,
            "S" => Suit::Spade,
            _ => panic!(),
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd)]
struct Card {
    rank: Rank,
    suit: Suit,
}

impl Card {
    fn from_text(text: &str) -> Self {
        Self {
            rank: Rank::from_text(&text[0..text.len() - 1]),
            suit: Suit::from_text(&text[text.len() - 1..]),
        }
    }
}

#[derive(PartialEq, PartialOrd, Eq, Ord)]
enum Category {
    HighCard(Rank, Rank, Rank, Rank, Rank),
    OnePair(Rank, Rank, Rank, Rank),
    TwoPairs(Rank, Rank, Rank),
    ThreeOfAKind(Rank, Rank, Rank),
    Straight(Rank),
    Flush(Rank, Rank, Rank, Rank, Rank),
    FullHouse(Rank, Rank),
    FourOfAKind(Rank, Rank),
    StraightFlush(Rank),
}

#[derive(Eq)]
struct Hand<'a> {
    text: &'a str,
    category: Category,
}

impl<'a> Hand<'a> {
    fn from_text(text: &'a str) -> Hand<'a> {
        // Parse cards into sorted list.
        let mut cards = text.split(' ').map(Card::from_text).collect::<Vec<_>>();
        cards.sort_unstable();

        // Determine the caregory of the hand.
        let is_flush = cards
            .iter()
            .zip(cards.iter().skip(1))
            .all(|(prev, next)| prev.suit == next.suit);
        let is_low_ace_straight = cards.iter().map(|card| &card.rank).eq([
            Rank::Two,
            Rank::Three,
            Rank::Four,
            Rank::Five,
            Rank::Ace,
        ]
        .iter());
        let is_straight = is_low_ace_straight
            || cards
                .iter()
                .zip(cards.iter().skip(1))
                .all(|(prev, next)| next.rank.to_index() == prev.rank.to_index() + 1);
        let category = if is_flush && is_straight {
            Category::StraightFlush(if is_low_ace_straight {
                cards[3].rank
            } else {
                cards[4].rank
            })
        } else {
            let mut rank_counts = vec![0; Rank::Ace.to_index() - Rank::Two.to_index() + 1];
            cards
                .iter()
                .for_each(|card| rank_counts[card.rank.to_index()] += 1);
            let mut count_ranks: [Vec<Rank>; 4] = std::array::from_fn(|_| Vec::new());
            for (index, &count) in rank_counts.iter().enumerate().rev() {
                if count > 0 {
                    count_ranks[count - 1].push(Rank::from_index(index))
                }
            }
            if let Some(&rank) = count_ranks[3].get(0) {
                Category::FourOfAKind(rank, count_ranks[0][0])
            } else if let (Some(&rank1), Some(&rank2)) =
                (count_ranks[2].get(0), count_ranks[1].get(0))
            {
                Category::FullHouse(rank1, rank2)
            } else if is_flush {
                Category::Flush(
                    cards[4].rank,
                    cards[3].rank,
                    cards[2].rank,
                    cards[1].rank,
                    cards[0].rank,
                )
            } else if is_straight {
                Category::Straight(if is_low_ace_straight {
                    cards[3].rank
                } else {
                    cards[4].rank
                })
            } else if let Some(&rank) = count_ranks[2].get(0) {
                Category::ThreeOfAKind(rank, count_ranks[0][0], count_ranks[0][1])
            } else if let (Some(&rank1), Some(&rank2)) =
                (count_ranks[1].get(0), count_ranks[1].get(1))
            {
                Category::TwoPairs(rank1, rank2, count_ranks[0][0])
            } else if let Some(&rank) = count_ranks[1].get(0) {
                Category::OnePair(
                    rank,
                    count_ranks[0][0],
                    count_ranks[0][1],
                    count_ranks[0][2],
                )
            } else {
                Category::HighCard(
                    count_ranks[0][0],
                    count_ranks[0][1],
                    count_ranks[0][2],
                    count_ranks[0][3],
                    count_ranks[0][4],
                )
            }
        };

        // Construct the hand object.
        Self { text, category }
    }
}

impl PartialEq for Hand<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.category == other.category
    }
}

impl PartialOrd for Hand<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for Hand<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.category.cmp(&other.category)
    }
}
