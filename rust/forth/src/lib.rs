use std::{collections::HashMap, rc::Rc};

pub type Value = i32;
pub type Result = std::result::Result<(), Error>;

pub struct Forth {
    stack: Vec<Value>,
    words: HashMap<String, Rc<Word>>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    DivisionByZero,
    StackUnderflow,
    UnknownWord,
    InvalidWord,
}

enum Token {
    Value(Value),
    Word(Rc<Word>),
}

enum Word {
    Add,
    Subtract,
    Multiply,
    Divide,
    Dup,
    Drop,
    Swap,
    Over,
    Custom(Vec<Token>),
}

impl Forth {
    pub fn new() -> Forth {
        Self {
            stack: Vec::new(),
            words: HashMap::from([
                ("+".into(), Rc::new(Word::Add)),
                ("-".into(), Rc::new(Word::Subtract)),
                ("*".into(), Rc::new(Word::Multiply)),
                ("/".into(), Rc::new(Word::Divide)),
                ("dup".into(), Rc::new(Word::Dup)),
                ("drop".into(), Rc::new(Word::Drop)),
                ("swap".into(), Rc::new(Word::Swap)),
                ("over".into(), Rc::new(Word::Over)),
            ]),
        }
    }

    pub fn stack(&self) -> &[Value] {
        &self.stack
    }

    pub fn eval(&mut self, input: &str) -> Result {
        let main_word = Self::parse_word(
            &mut input.split(' ').filter(|token| !token.is_empty()),
            &mut self.words,
        )?;
        self.eval_word(main_word.as_ref())
    }

    fn parse_word(
        string_tokens: &mut dyn Iterator<Item = &str>,
        words: &mut HashMap<String, Rc<Word>>,
    ) -> std::result::Result<Rc<Word>, Error> {
        let mut tokens = Vec::new();
        while let Some(string_token) = string_tokens.next() {
            match string_token {
                ";" => break,
                ":" => {
                    let word_name_token = string_tokens.next().unwrap();
                    match word_name_token.parse::<Value>() {
                        Ok(_) => return Err(Error::InvalidWord),
                        Err(_) => {
                            words.insert(
                                word_name_token.to_lowercase(),
                                // TODO: Avoid cloning the `words` map by implementing some sort of
                                // versioning.
                                Self::parse_word(string_tokens, &mut words.clone())?,
                            );
                        }
                    }
                }
                _ => match string_token.parse::<Value>() {
                    Ok(value) => {
                        tokens.push(Token::Value(value));
                    }
                    Err(_) => match words.get(&string_token.to_lowercase()) {
                        Some(word) => tokens.push(Token::Word(Rc::clone(word))),
                        None => return Err(Error::UnknownWord),
                    },
                },
            }
        }
        Ok(Rc::new(Word::Custom(tokens)))
    }

    fn eval_word(&mut self, word: &Word) -> Result {
        match word {
            Word::Add => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => {
                    self.stack.push(n1 + n2);
                    Ok(())
                }
                _ => Err(Error::StackUnderflow),
            },
            Word::Subtract => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => {
                    self.stack.push(n1 - n2);
                    Ok(())
                }
                _ => Err(Error::StackUnderflow),
            },
            Word::Multiply => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => {
                    self.stack.push(n1 * n2);
                    Ok(())
                }
                _ => Err(Error::StackUnderflow),
            },
            Word::Divide => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => match n2 {
                    0 => Err(Error::DivisionByZero),
                    n2 => {
                        self.stack.push(n1 / n2);
                        Ok(())
                    }
                },
                _ => Err(Error::StackUnderflow),
            },
            Word::Dup => match self.stack.last() {
                Some(&n) => {
                    self.stack.push(n);
                    Ok(())
                }
                None => Err(Error::StackUnderflow),
            },
            Word::Drop => match self.stack.pop() {
                Some(_) => Ok(()),
                None => Err(Error::StackUnderflow),
            },
            Word::Swap => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => {
                    self.stack.extend_from_slice(&[n2, n1]);
                    Ok(())
                }
                _ => Err(Error::StackUnderflow),
            },
            Word::Over => match (self.stack.pop(), self.stack.pop()) {
                (Some(n2), Some(n1)) => {
                    self.stack.extend_from_slice(&[n1, n2, n1]);
                    Ok(())
                }
                _ => Err(Error::StackUnderflow),
            },
            Word::Custom(tokens) => {
                for token in tokens {
                    match token {
                        &Token::Value(value) => self.stack.push(value),
                        Token::Word(word) => self.eval_word(word.as_ref())?,
                    }
                }
                Ok(())
            }
        }
    }
}

impl Default for Forth {
    fn default() -> Self {
        Self::new()
    }
}
