use std::collections::HashMap;
use std::thread;

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    thread::scope(|s| {
        let mut threads = Vec::with_capacity(worker_count);
        for chunk in input.chunks(input.len().max(1).div_ceil(worker_count)) {
            threads.push(s.spawn(move || -> HashMap<char, usize> {
                chunk.iter().fold(HashMap::new(), |mut counts, input| {
                    input
                        .chars()
                        .filter_map(|c| match c.is_alphabetic() {
                            true => c.to_lowercase().next(),
                            false => None,
                        })
                        .for_each(|ch| {
                            *counts.entry(ch).or_default() += 1;
                        });
                    counts
                })
            }));
        }
        threads
            .into_iter()
            .fold(HashMap::new(), |mut counts, thread| {
                thread.join().unwrap().into_iter().for_each(|(key, value)| {
                    *counts.entry(key).or_default() += value;
                });
                counts
            })
    })
}
