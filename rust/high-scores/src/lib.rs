pub struct HighScores<'a> {
    scores: &'a [u32],
    best: Vec<u32>,
}

impl HighScores<'_> {
    pub fn new(scores: &[u32]) -> HighScores {
        let mut best = Vec::new();
        for &n in scores {
            if best.len() < 3 {
                best.push(n);
            } else if n > best[2] {
                best[2] = n;
            }
            let mut i = best.len() - 1;
            while i > 0 && best[i] > best[i - 1] {
                best.swap(i, i - 1);
                i -= 1;
            }
        }
        HighScores { scores, best }
    }

    pub fn scores(&self) -> &[u32] {
        self.scores
    }

    pub fn latest(&self) -> Option<u32> {
        self.scores.last().copied()
    }

    pub fn personal_best(&self) -> Option<u32> {
        self.best.first().copied()
    }

    pub fn personal_top_three(&self) -> Vec<u32> {
        self.best.clone()
    }
}
