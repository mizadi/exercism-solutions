pub fn abbreviate(phrase: &str) -> String {
    let mut result = String::new();
    let mut sep = true;
    let mut lower = false;
    for ch in phrase.chars() {
        if ch.is_whitespace() || ch == '-' {
            sep = true;
        } else if ch.is_lowercase() {
            if sep {
                result.push(ch.to_ascii_uppercase());
            }
            sep = false;
            lower = true;
        } else if ch.is_uppercase() {
            if sep || lower {
                result.push(ch);
            }
            sep = false;
            lower = false;
        }
    }
    result
}
