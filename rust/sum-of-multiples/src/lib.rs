pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {
    let mut iters = factors
        .iter()
        .filter_map(|&factor| match factor {
            0 => None,
            _ => Some((factor..limit).step_by(factor as usize).peekable()),
        })
        .collect::<Vec<_>>();
    let mut sum = 0;
    loop {
        let mut min = None;
        for iter in iters.iter_mut() {
            if let Some(&head) = iter.peek() {
                min = min.min(Some(head)).or(Some(head));
            }
        }
        match min {
            Some(min) => {
                sum += min;
                for iter in iters.iter_mut() {
                    while let Some(&head) = iter.peek() {
                        if head != min {
                            break;
                        }
                        iter.next();
                    }
                }
            }
            None => break sum,
        }
    }
}
