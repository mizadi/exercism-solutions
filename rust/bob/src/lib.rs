pub fn reply(message: &str) -> &str {
    match message.trim() {
        "" => "Fine. Be that way!",
        message => {
            let is_question = message.ends_with('?');
            let is_yell = message
                .chars()
                .filter(|c| c.is_alphabetic())
                .try_fold(
                    false,
                    |_, c| if c.is_uppercase() { Some(true) } else { None },
                )
                == Some(true);
            match (is_question, is_yell) {
                (true, true) => "Calm down, I know what I'm doing!",
                (true, false) => "Sure.",
                (false, true) => "Whoa, chill out!",
                (false, false) => "Whatever.",
            }
        }
    }
}
