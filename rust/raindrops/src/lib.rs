pub fn raindrops(n: u32) -> String {
    let result = [(3, "Pling"), (5, "Plang"), (7, "Plong")].into_iter().fold(
        String::new(),
        |mut result, (d, text)| {
            if n % d == 0 {
                result.push_str(text);
            }
            result
        },
    );
    match result.is_empty() {
        true => n.to_string(),
        false => result,
    }
}
