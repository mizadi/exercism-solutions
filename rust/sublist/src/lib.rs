#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(first_list: &[T], second_list: &[T]) -> Comparison {
    match (
        subseq(first_list, second_list),
        subseq(second_list, first_list),
    ) {
        (true, true) => Comparison::Equal,
        (true, false) => Comparison::Sublist,
        (false, true) => Comparison::Superlist,
        (false, false) => Comparison::Unequal,
    }
}

fn subseq<T: PartialEq>(lhs: &[T], rhs: &[T]) -> bool {
    lhs.len() <= rhs.len()
        && (0..=rhs.len() - lhs.len()).any(|i| (0..lhs.len()).all(|j| lhs[j] == rhs[i + j]))
}
