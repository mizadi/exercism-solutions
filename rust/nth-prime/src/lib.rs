pub fn nth(n: u32) -> u32 {
    (2..)
        .filter(|&n| (2..).take_while(|k| k * k <= n).all(|k| n % k != 0))
        .nth(n as usize)
        .unwrap()
}
