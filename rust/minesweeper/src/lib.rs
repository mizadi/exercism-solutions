const ADJACAENT_DELTAS: [(isize, isize); 8] = [
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (0, -1),
    (0, 1),
    (1, -1),
    (1, 0),
    (1, 1),
];

pub fn annotate(minefield: &[&str]) -> Vec<String> {
    minefield
        .iter()
        .enumerate()
        .map(|(row_index, row)| {
            row.as_bytes()
                .iter()
                .enumerate()
                .map(|(cell_index, cell)| match cell {
                    b'*' => '*',
                    _ => match ADJACAENT_DELTAS
                        .iter()
                        .map(|&(dr, dc)| (row_index as isize + dr, cell_index as isize + dc))
                        .filter(|&(r, c)| {
                            r >= 0
                                && c >= 0
                                && (r as usize) < minefield.len()
                                && (c as usize) < minefield[r as usize].len()
                                && minefield[r as usize].as_bytes()[c as usize] == b'*'
                        })
                        .count()
                    {
                        0 => ' ',
                        n => (n as u8 + b'0') as char,
                    },
                })
                .collect()
        })
        .collect()
}
