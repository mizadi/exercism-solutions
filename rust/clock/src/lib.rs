use std::fmt::Display;

#[derive(PartialEq, Debug)]
pub struct Clock {
    minutes: u16,
}

impl Clock {
    pub fn new(hours: i32, minutes: i32) -> Self {
        dbg!(Self {
            minutes: (hours * 60 + minutes).rem_euclid(24 * 60) as u16,
        })
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Clock::new(0, self.minutes as i32 + minutes)
    }
}

impl Display for Clock {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:02}:{:02}", self.minutes / 60, self.minutes % 60)
    }
}

impl From<Clock> for String {
    fn from(value: Clock) -> Self {
        value.to_string()
    }
}
