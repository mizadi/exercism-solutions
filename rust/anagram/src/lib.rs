use std::collections::{HashMap, HashSet};
use unicode_segmentation::UnicodeSegmentation;

fn count_graphemes<'a>(word: &'a str) -> HashMap<&'a str, usize> {
    word.graphemes(true)
        .fold(HashMap::new(), |mut counts, grapheme| {
            counts.entry(grapheme).and_modify(|v| *v += 1).or_insert(1);
            counts
        })
}

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &[&'a str]) -> HashSet<&'a str> {
    let word_l = word.to_lowercase();
    let target_counts = count_graphemes(&word_l);
    possible_anagrams
        .iter()
        .filter_map(|possible| {
            let possible_l = possible.to_lowercase();
            if possible_l != word_l && count_graphemes(&possible_l) == target_counts {
                Some(*possible)
            } else {
                None
            }
        })
        .collect()
}
